<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;
use Response;
class FacebookLogin extends Controller
{
    public function store(Request $request)
    {
        $user = new User([
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'password' => $request->get('password')
        ]);
        $user->save();
        return Response::json(array('success' => true, 'last_insert_id' => $user->id), 200);
    }
}
