
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */


const app = new Vue({
    el: '#app',
    methods: {
        openFbLoginDialog () {
            FB.login(this.checkLoginState, { scope: 'email' })
        },
        checkLoginState: function (response) {
            if (response.status === 'connected') {
                FB.api('/me', { fields: 'name,email' }, function(profile) {
                    var userData = {
                        name: "John",
                        email: "john@gmail.com",
                        password:"11ddbaf3386aea1f2974eee984542152"
                    }
                    this.createUser(userData);
                });
            } else if (response.status === 'not_authorized') {
                // the user is logged in to Facebook,
                // but has not authenticated your app
            } else {
                // the user isn't logged in to Facebook.
            }
        },
        createUser(userData) {
            axios.post('/users', userData)
                .then((res) => {

                })
                .catch((err) => console.error(err));
        },
    }
});

