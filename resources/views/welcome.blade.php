<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">
        <script
                src="https://code.jquery.com/jquery-3.3.1.min.js"
                integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
                crossorigin="anonymous"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" />

        <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js"></script>
        {{--<script src="https://unpkg.com/vue"></script>--}}
        <script>
            window.fbAsyncInit = function() {
                FB.init({
                    appId            : '537130266709845',
                    autoLogAppEvents : true,
                    xfbml            : true,
                    version          : 'v2.10'
                });
                FB.AppEvents.logPageView();
            };

            (function(d, s, id){
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id)) {return;}
                js = d.createElement(s); js.id = id;
                js.src = "//connect.facebook.net/en_US/sdk.js";
                fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));
        </script>

        <!-- Styles -->
        <style>
            html,
            body {
                height: 100%;
            }

            body {
                color: #333;
            }

            .site-wrapper {
                display: table;
                width: 100%;
                height: 100%; /* For at least Firefox */
                min-height: 100%;
            }
            .site-wrapper-inner {
                display: table-cell;
                vertical-align: top;
                border: 1px solid #ccc;
            }

            .inner {
                padding: 2rem;
            }

            .inner.cover {
                border: 1px solid #f0f0f0;
                padding: 2.5rem;
            }

            .cover {
                padding: 0 1.5rem;
            }
            .cover .btn-lg {
                padding: .75rem 1.25rem;
                font-weight: bold;
            }

            @media (min-width: 40em) {
                .site-wrapper-inner {
                    vertical-align: middle;
                }

                .cover-container {
                    width: 100%;
                }
            }

            @media (min-width: 62em) {
                .cover-container {
                    width: 42rem;
                }
            }
        </style>
    </head>
    <body >
    <div id="app" class="site-wrapper"><!-- site-wrapper -->
        <div class="site-wrapper-inner"><!-- site-wrapper-inner -->
            <div class="container"><!-- container -->
                <div class="row justify-content-md-center">
                    <div class="col-md-5">

                        <div class="inner cover">
                            <button type="button" class="btn btn-primary btn-block" @click="openFbLoginDialog">Facebook Login</button>
                        </div>
                    </div>
                </div>
            </div><!-- container -->
        </div><!-- site-wrapper-inner -->
    </div><!-- site-wrapper -->
    </body>
    <script type="text/javascript" src="{{ asset('js/app.js') }}"></script>
</html>
